/**
 * 
 */
package com.chess.models;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.chess.master.Heroism;

/**
 * @author polo
 *
 */
public class BishopTest {

	List<String> expectedRoute;
	List<String> actualRoute;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		expectedRoute = new ArrayList<String>();
		expectedRoute.add("D1");
		expectedRoute.add("D2");
		expectedRoute.add("E2");
		expectedRoute.add("F2");
		expectedRoute.add("F1");
	}

	/**
	 * Test method for
	 * {@link com.chess.models.Bishop#move(com.chess.models.Location)}.
	 */
	@Test
	public void testKingMove() {
		Heroism king = new King();
		Location loc = new Location(5, 1); // E1 == 5,1
		actualRoute = king.move(loc);

		assertTrue(actualRoute.size() == expectedRoute.size() && actualRoute.containsAll(expectedRoute)
				&& expectedRoute.containsAll(actualRoute));
	}

}
