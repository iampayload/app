package com.chess.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.chess.master.Heroism;
import com.chess.util.ChessUtil;

public class Horse implements Heroism {

	public enum HORSE {
		NNE, NNW, SSE, SSW, EEN, EES, WWN, WWS
	};

	List<String> moveList = null;

	@Override
	public List<String> move(Location source) {

		moveList = new ArrayList<>();
		for (HORSE kk : Arrays.asList(HORSE.values())) {
			String[] pp = kk.toString().split("");
			Location dest = new Location(source.getX(), source.getY());
			dest = ChessUtil.nextDestination(dest, pp);
			if (ChessUtil.checkExit(dest)) {
				continue;
			}
			moveList.add(String.valueOf((char) (dest.getX() + 64) + "" + dest.getY()));
			System.out.println(String.valueOf((char) (dest.getX() + 64) + "" + dest.getY()));
		}
		return moveList;
	}

}
