package com.chess.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.chess.master.Heroism;
import com.chess.util.ChessUtil;

public class Bishop implements Heroism {

	public enum BISHOP {
		NE, SE, NW, SW
	};

	List<String> moveList = null;

	@Override
	public List<String> move(Location source) {
		moveList = new ArrayList<String>();
		for (BISHOP kk : Arrays.asList(BISHOP.values())) {

			String[] pp = kk.toString().split("");
			Location dest = new Location(source.getX(), source.getY());
			while (ChessUtil.check(dest)) {
				dest = ChessUtil.nextDestination(dest, pp);
				if (ChessUtil.checkExit(dest)) {
					continue;
				}
				moveList.add(String.valueOf((char) (dest.getX() + 64) + "" + dest.getY()));
				System.out.println(String.valueOf((char) (dest.getX() + 64) + "" + dest.getY()));
			}
		}
		return moveList;

	}

}
