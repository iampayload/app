package com.chess.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.chess.master.Heroism;
import com.chess.util.ChessUtil;

public class Pawn implements Heroism {

	public enum PAWN {
		N, S, E, W, NE, SE, NW, SW
	};

	List<String> moveList = null;

	public List<String> move(Location source) {

		moveList = new ArrayList<>();
		for (PAWN kk : Arrays.asList(PAWN.values())) {
			String[] pp = kk.toString().split("");
			Location dest = new Location(source.getX(), source.getY());
			dest = ChessUtil.nextDestination(dest, pp);
			if (ChessUtil.checkExit(dest)) {
				continue;
			}
			moveList.add(String.valueOf((char) (dest.getX() + 64) + "" + dest.getY()));
			System.out.println(String.valueOf((char) (dest.getX() + 64) + "" + dest.getY()));
		}
		return moveList;
	}
}
