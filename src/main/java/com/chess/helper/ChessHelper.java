package com.chess.helper;

import static java.lang.Integer.parseInt;

import com.chess.master.Heroism;
import com.chess.models.Location;

public class ChessHelper {

	Location location = null;
	Heroism hero = null;
	int x = 0;
	int y = 0;

	public ChessHelper(String heroname, String loc) {
		hero = HeroFactory.createHero(heroname);

		x = (int) String.valueOf(loc.split("")[0]).charAt(0) - 64;
		y = parseInt(loc.split("")[1]);

		location = new Location(x, y);
	}

	public void action() {
		if (null != hero) {

			hero.move(location);

		}

	}

}
