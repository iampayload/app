package com.chess.helper;

import com.chess.master.Heroism;
import com.chess.models.Bishop;
import com.chess.models.Horse;
import com.chess.models.King;
import com.chess.models.Queen;
import com.chess.models.Rook;

public class HeroFactory {

	public static Heroism createHero(String heroName) {
		if (heroName.equalsIgnoreCase("queen")) {
			return new Queen();
		} else if ((heroName.equalsIgnoreCase("horse"))) {
			return new Horse();
		} else if ((heroName.equalsIgnoreCase("king"))) {
			return new King();
		}else if ((heroName.equalsIgnoreCase("bishop"))) {
			return new Bishop();
		}else if ((heroName.equalsIgnoreCase("rook"))) {
			return new Rook();
		}
		throw new IllegalArgumentException("No such Hero");
	}
}
