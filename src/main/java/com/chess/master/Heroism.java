package com.chess.master;

import java.util.List;

import com.chess.models.Location;

public interface Heroism {

	List<String> move(Location loc);
}
