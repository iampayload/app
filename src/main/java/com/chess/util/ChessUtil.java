package com.chess.util;

import com.chess.models.Location;

public class ChessUtil {

	public static boolean check(Location d) {
		int x = d.getX();
		int y = d.getY();

		return x > 1 && y > 1 && x < 8 && y < 8;
	}

	public static Location nextDestination(Location dest, String[] pp) {

		for (String season : pp) {
			if ("n".equalsIgnoreCase(season)) {
				dest.setY(dest.getY() + 1);
			} else if ("e".equalsIgnoreCase(season)) {
				dest.setX(dest.getX() + 1);
			} else if ("w".equalsIgnoreCase(season)) {
				dest.setX(dest.getX() - 1);
			} else if ("s".equalsIgnoreCase(season)) {
				dest.setY(dest.getY() - 1);
			}
		}
		return dest;
	}

	public static boolean checkExit(Location dest) {
		return dest.getY() == 0 || dest.getY() == 9 || dest.getX() == 0 || dest.getX() == 9;
	}
}
